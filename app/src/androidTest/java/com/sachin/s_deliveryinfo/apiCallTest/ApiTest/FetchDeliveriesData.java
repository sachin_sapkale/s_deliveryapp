package com.sachin.s_deliveryinfo.apiCallTest.ApiTest;


import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.android.volley.VolleyError;
import com.sachin.s_deliveryinfo.Volley.VolleyObject;
import com.sachin.s_deliveryinfo.Volley.VolleyResponseInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

@RunWith(AndroidJUnit4.class)
public class FetchDeliveriesData {

    private Context mcontext;
    private static String TAG = "@TEST|| " + FetchDeliveriesData.class.getName() + " || ";
    private JSONArray responseData;


    @Before
    public void getApplicationContext() throws Exception {
        mcontext = InstrumentationRegistry.getTargetContext();
        assertNotNull("context is null", mcontext);
    }

    @Test
    public void loadServices() {
        try {
            FetchDeliveriesData();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void FetchDeliveriesData() throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        VolleyObject.getSDKconfigInstance().fetchDeliveryData(new VolleyResponseInterface() {
            @Override
            public void onResponse(JSONArray response, VolleyError volleyError) {
                assertNotNull(TAG + "Response is not null ", response);
                assertNull("Error is null", volleyError);
                responseData = response;
                latch.countDown();
            }
        });
        try {
            latch.await();
            assertNotNull(responseData);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
