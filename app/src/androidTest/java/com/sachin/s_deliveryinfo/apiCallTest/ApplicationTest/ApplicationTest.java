package com.sachin.s_deliveryinfo.apiCallTest.ApplicationTest;

import android.test.ApplicationTestCase;
import android.util.Log;
import com.sachin.s_deliveryinfo.Controller.MainApplication;
import com.sachin.s_deliveryinfo.Volley.MyVolleySingleton;
import com.sachin.s_deliveryinfo.Volley.VolleyObject;


public class ApplicationTest extends ApplicationTestCase<MainApplication> {

    public ApplicationTest() {
                super(MainApplication.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        createApplication();
        VolleyObject.initialize(getContext());
        assertNotNull("ApplicationTest(MainApplication is null)", MyVolleySingleton.getInstance(getContext()));

        Log.d("@TEST|| " , "-------ApplicationTest---");

        //to terminate the application - terminateApplication();
    }
}