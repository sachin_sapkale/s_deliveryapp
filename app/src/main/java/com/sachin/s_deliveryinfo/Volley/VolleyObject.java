package com.sachin.s_deliveryinfo.Volley;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.sachin.s_deliveryinfo.Utilities.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyObject {

    private static Context mcontext;
    private static MyVolleySingleton myVolleySingleton;
    private static VolleyObject volleyObject;


    public static void initialize(Context context) {
        mcontext = context.getApplicationContext();
        myVolleySingleton = new MyVolleySingleton(mcontext);
    }

    public void fetchDeliveryData(final VolleyResponseInterface volleyResponseInterface) {

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, AppConstants.URL_DELIVERIES_ARRAY, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                volleyResponseInterface.onResponse(response, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyResponseInterface.onResponse(null, error);
            }
        });

        //Retry Policy
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        myVolleySingleton.addToRequestQueue(jsonArrayRequest);
    }

    public static VolleyObject getSDKconfigInstance() {

        try {
            if (volleyObject == null) {
                volleyObject = new VolleyObject();
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        return volleyObject;
    }
}
