package com.sachin.s_deliveryinfo.Volley;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface VolleyObjectResponseInterface {

    void onResponse(JSONObject response, VolleyError volleyError);

}
