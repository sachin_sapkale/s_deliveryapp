package com.sachin.s_deliveryinfo.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sachin.s_deliveryinfo.Activities.MainActivity;
import com.sachin.s_deliveryinfo.Model.DeliveryItemDetail;
import com.sachin.s_deliveryinfo.R;
import com.sachin.s_deliveryinfo.Utilities.TypeFaceHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListBaseAdapter extends BaseAdapter {

    private final Context context;
    private final Picasso picasso;
    private final Typeface typeFace_FA, typeFace_Bold, typeFace_Light;
    private ArrayList<DeliveryItemDetail> deliveryItemDetails;
    private LayoutInflater inflater;

    public ListBaseAdapter(Context act, ArrayList<DeliveryItemDetail> details) {

        this.context = act;
        this.deliveryItemDetails = details;
        this.picasso = Picasso.get();
        this.typeFace_FA = TypeFaceHelper.getInstance(context).getStyleTypeFace(TypeFaceHelper.FONT_AWESOME);
        this.typeFace_Bold = TypeFaceHelper.getInstance(context).getStyleTypeFace(TypeFaceHelper.FiraSansBold);
        this.typeFace_Light = TypeFaceHelper.getInstance(context).getStyleTypeFace(TypeFaceHelper.FiraSansLight);
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return deliveryItemDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View itemView = inflater.inflate(R.layout.rec_item_list, parent, false);
        TextView rec_item_title = (TextView) itemView.findViewById(R.id.rec_item_title);
        TextView rec_item_desc = (TextView) itemView.findViewById(R.id.rec_item_desc);
        final ImageView rec_item_imageview = (ImageView) itemView.findViewById(R.id.rec_item_imageview);
        RelativeLayout rec_parent_rel = (RelativeLayout) itemView.findViewById(R.id.rec_parent_rel);

        rec_item_title.setText(deliveryItemDetails.get(position).description);
        rec_item_title.setTypeface(TypeFaceHelper.getInstance(context).getStyleTypeFace(TypeFaceHelper.FiraSansBold));
        rec_item_desc.setText(deliveryItemDetails.get(position).address);
        rec_item_desc.setTypeface(TypeFaceHelper.getInstance(context).getStyleTypeFace(TypeFaceHelper.FiraSansLight));
        Picasso.get().load(deliveryItemDetails.get(position).imageUrl)
                .placeholder(ContextCompat.getDrawable(context,R.drawable.logo))
                .into(rec_item_imageview, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(deliveryItemDetails.get(position).imageUrl)
                                .networkPolicy(NetworkPolicy.OFFLINE)
                                .placeholder(ContextCompat.getDrawable(context,R.drawable.logo))
                                .into(rec_item_imageview);
                    }
                });
        rec_parent_rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)context).loadSingleDetailFragment(deliveryItemDetails.get(position));
            }
        });


        return itemView;
    }
}
