package com.sachin.s_deliveryinfo.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sachin.s_deliveryinfo.Activities.MainActivity;
import com.sachin.s_deliveryinfo.Adapter.ListBaseAdapter;
import com.sachin.s_deliveryinfo.MVP.Presenter;
import com.sachin.s_deliveryinfo.Model.DeliveryItemDetail;
import com.sachin.s_deliveryinfo.R;
import com.sachin.s_deliveryinfo.Utilities.AppUtilities;
import com.sachin.s_deliveryinfo.Utilities.CircleTransform;
import com.sachin.s_deliveryinfo.Utilities.TypeFaceHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ListFragment extends Fragment implements View.OnClickListener {
    private Context mcontext;
    public static ProgressDialog mProgressDialog;
    private Presenter presenter;
    private ListView recyclerViewList;
    private TextView viewtype;
    private TextView textTitle;
    private String detailItemModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mcontext = context;
    }
    public static ListFragment newInstance(String d_id) {
        ListFragment f = new ListFragment();

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("data", d_id);
        f.setArguments(args);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        detailItemModel = getArguments().getString("data","");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            initialize(view);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void initialize(View view) throws JSONException {
//        initProgressbar();
        viewtype = (TextView)view.findViewById(R.id.viewtype);
        textTitle = (TextView) view.findViewById(R.id.titleTV);
        textTitle.setTypeface(TypeFaceHelper.getInstance(mcontext).getStyleTypeFace(TypeFaceHelper.FiraSansBold));
        viewtype.setTypeface(TypeFaceHelper.getInstance(mcontext).getStyleTypeFace(TypeFaceHelper.FiraSansBold));
        viewtype.setOnClickListener(this);

        recyclerViewList = (ListView) view.findViewById(R.id.rec_list);
//        showProgress("Loading,Please wait...");
        if(!TextUtils.isEmpty(detailItemModel)) {
            JSONArray jsonArray = new JSONArray(detailItemModel);
            ParseJsonDataAsync parseJsonDataAsync = new ParseJsonDataAsync();
            parseJsonDataAsync.execute(jsonArray);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.viewtype:
                if(viewtype.getText().toString().toLowerCase().equalsIgnoreCase("mapview")){
                    recyclerViewList.setVisibility(View.GONE);
                    viewtype.setText("ListView");
                }else{
                    recyclerViewList.setVisibility(View.VISIBLE);
                    viewtype.setText("MapView");
                }
                break;
        }
    }

    public class ParseJsonDataAsync extends AsyncTask<JSONArray, Void, ArrayList<DeliveryItemDetail>> {

        @Override
        protected ArrayList<DeliveryItemDetail> doInBackground(JSONArray... jsonArrays) {
            if (jsonArrays != null && jsonArrays[0]!=null && jsonArrays[0].length() > 0) {
                ArrayList<DeliveryItemDetail> deliveryItemDetailsList = new ArrayList<>();
                for (int i = 0; i < jsonArrays[0].length(); i++) {
                    JSONObject jsonObject = jsonArrays[0].optJSONObject(i);
                    if (jsonObject != null) {
                        DeliveryItemDetail deliveryItemDetail = new DeliveryItemDetail(jsonObject);
                        deliveryItemDetailsList.add(deliveryItemDetail);
                    }
                }
                if (deliveryItemDetailsList != null && deliveryItemDetailsList.size() > 0) {
                    return deliveryItemDetailsList;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(final ArrayList<DeliveryItemDetail> deliveryItemDetails) {
            super.onPostExecute(deliveryItemDetails);
            if (deliveryItemDetails != null && deliveryItemDetails.size() > 0) {
                recyclerViewList.setVisibility(View.VISIBLE);
//                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mcontext);
                Collections.sort(deliveryItemDetails, new Comparator<DeliveryItemDetail>() {
                    @Override
                    public int compare(DeliveryItemDetail lhs, DeliveryItemDetail rhs) {
                        return Integer.toString(lhs.id).compareTo(Integer.toString(rhs.id));
                    }
                });

                ListBaseAdapter listRecyclerAdapter = new ListBaseAdapter(mcontext, deliveryItemDetails);
//                recyclerViewList.setLayoutManager(linearLayoutManager);
                recyclerViewList.setAdapter(listRecyclerAdapter);
                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for(int i = 0; i < deliveryItemDetails.size(); i++){
                            LatLng addr = new LatLng(deliveryItemDetails.get(i).lat, deliveryItemDetails.get(i).lng);
                            Marker myMarker = googleMap.addMarker(new MarkerOptions().position(addr)
                                    .title(deliveryItemDetails.get(i).address)
                            .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(deliveryItemDetails.get(i).address,deliveryItemDetails.get(i).imageUrl))));
                            myMarker.setTitle(deliveryItemDetails.get(i).address);
                            myMarker.setTag(i);
                            builder.include(addr);
                        }
                        LatLngBounds bounds = builder.build();
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));
                        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                int getTag = (int) marker.getTag();
                                if(getTag<=deliveryItemDetails.size()){
                                    ((MainActivity)mcontext).loadSingleDetailFragment(deliveryItemDetails.get(getTag));
                                }
                                return true;
                            }
                        });
                    }
                });
            } else {
                recyclerViewList.setVisibility(View.GONE);
                AppUtilities.showAlertDialog(mcontext, "Oops!", "Something went wrong...");
            }
        }
    }
    private Bitmap getMarkerBitmapFromView(String title, final String url) {
//        title = title.replace(" ","\n");

        View customMarkerView = ((LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_main, null);
//        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.img);
        ImageView lineColorCode = (ImageView)customMarkerView.findViewById(R.id.ctt);
        int color = Color.parseColor("#f16622");
        lineColorCode.setColorFilter(color);
        final ImageView imgPerson = (ImageView) customMarkerView.findViewById(R.id.imgPerson);
        Picasso.get().load(url)
                .transform(new CircleTransform())
                .placeholder(ContextCompat.getDrawable(mcontext,R.drawable.logo))
                .into(imgPerson, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(url)
                                .transform(new CircleTransform())
                                .networkPolicy(NetworkPolicy.OFFLINE)
                                .placeholder(ContextCompat.getDrawable(mcontext,R.drawable.logo))
                                .into(imgPerson);
                    }
                });
        TextView name = (TextView) customMarkerView.findViewById(R.id.name);
        name.setText(title);
        name.setTypeface(TypeFaceHelper.getInstance(mcontext).getStyleTypeFace(TypeFaceHelper.FiraSansLight));
//        markerImageView.setImageResource(R.drawable.ca);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }
}
