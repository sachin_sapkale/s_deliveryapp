package com.sachin.s_deliveryinfo.Controller;

import android.app.Application;
import android.content.Context;

import com.sachin.s_deliveryinfo.Volley.VolleyObject;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainApplication extends Application {


    public static Context mcontext;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mcontext = this;
        VolleyObject.initialize(mcontext);
    }



}
