package com.sachin.s_deliveryinfo.MVP;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.VolleyError;
import com.sachin.s_deliveryinfo.Utilities.AppUtilities;
import com.sachin.s_deliveryinfo.Volley.VolleyObject;
import com.sachin.s_deliveryinfo.Volley.VolleyResponseInterface;

import org.json.JSONArray;


public class Presenter implements MainMVP.presenter {

    private final MainMVP.view mainview;
    private ProgressDialog mProgressDialog;
    private Context mcontext;

    public Presenter(MainMVP.view view, Context context) {
        mainview = view;
        mcontext = context;
    }


    @Override
    public void displayDialogError(VolleyError volleyError) {
        mainview.displayDialogError(volleyError);
    }

    @Override
    public void getdata() {
            VolleyObject.getSDKconfigInstance().fetchDeliveryData(new VolleyResponseInterface() {
                @Override
                public void onResponse(JSONArray response, VolleyError volleyError) {
                    if (volleyError == null) {
                        mainview.getdata(response);
                    } else {
                        mainview.displayDialogError(volleyError);
                    }
                }
            });
    }
}
