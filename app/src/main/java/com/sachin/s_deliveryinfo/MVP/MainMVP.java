package com.sachin.s_deliveryinfo.MVP;

import android.content.Context;

import com.android.volley.VolleyError;

import org.json.JSONArray;


public interface MainMVP {
    interface view{
        void displayDialogError(VolleyError volleyError);
        void getdata(JSONArray jsonArray);
    }

    interface presenter{
        void displayDialogError(VolleyError volleyError);
        void getdata();
    }
}
