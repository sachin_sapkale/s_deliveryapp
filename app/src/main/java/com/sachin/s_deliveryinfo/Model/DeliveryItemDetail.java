package com.sachin.s_deliveryinfo.Model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class DeliveryItemDetail implements Parcelable{

    public int id;
    public String description;
    public String imageUrl;
    public float lat;
    public float lng;
    public String address;

    public DeliveryItemDetail(JSONObject jsonObject){
        id = jsonObject.optInt("id");
        description = jsonObject.optString("description");
        imageUrl = jsonObject.optString("imageUrl");
        if(jsonObject.has("location")){
            JSONObject jsonObject1 = jsonObject.optJSONObject("location");
            if(jsonObject1!=null){
                lat = (float) jsonObject1.optDouble("lat");
                lng = (float) jsonObject1.optDouble("lng");
                address = jsonObject1.optString("address");
            }
        }

    }

    public DeliveryItemDetail(Parcel in){

        this.id = in.readInt();
        this.description = in.readString();
        this.imageUrl =in.readString();
        this.lat =in.readInt();
        this.lng =in.readInt();
        this.address =in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.description);
        dest.writeString(this.imageUrl);
        dest.writeFloat(this.lat);
        dest.writeFloat(this.lng);
        dest.writeString(this.address);
    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public DeliveryItemDetail createFromParcel(Parcel in) {
            return new DeliveryItemDetail(in);
        }

        public DeliveryItemDetail[] newArray(int size) {
            return new DeliveryItemDetail[size];
        }
    };


    public static void writeObject(Context context, String key, String object) throws IOException {
        ObjectOutput out = new ObjectOutputStream(new FileOutputStream(new File(context.getCacheDir(),"")+key+".srl"));
        out.writeObject( object );
        out.close();
    }

    public static Object readObject(Context context, String key) throws IOException,
            ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(new File(context.getCacheDir(),"")+key+".srl")));
        String jsonObject = (String) in.readObject();
        in.close();
        return jsonObject;
    }
}
