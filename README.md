DeliApp (Delivery Information App)

App Includes the following:
-List and Map View of the list of deliveries.
-Detail of single delivery item on click from list.
-MVP pattern.

-Used volley library for api calling and singleton.
-Used Cached memory for storing data offline.
-Instrumentation Test case written
-Add fabric for crash and exception handling.
-Picasso for image loading and caching.
-To use custom key, change key from google_maps_api.xml